# Matrix Encryption (2016)



## Description

Archived matrix encryption project from 2016.


## Getting Started

### Dependencies

* C Standard Library, GCC
* Linux


### Usage

* Compile matrix_encryption.c
* Run executable
* Compile matrix_decryption.c
* Run executable


## Author

Austin Richie

https://gitlab.com/ar39907/
