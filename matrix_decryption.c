#include <stdio.h>

int Alpha_Convert(char m){
	char x = m;
	int y = 0;
	if(x == 'A'){
		y = 1;
		return y;
	}else if(x == 'B'){
		y = 2;
		return y;
	}else if(x == 'C'){
		y = 3;
		return y;
	}else if(x == 'D'){
		y = 4;
		return y;
	}else if(x == 'E'){
		y = 5;
		return y;
	}else if(x == 'F'){
		y = 6;
		return y;
	}else if(x == 'G'){
		y = 7;
		return y;
	}else if(x == 'H'){
		y = 8;
		return y;
	}else if(x == 'I'){
		y = 9;
		return y;
	}else if(x == 'J'){
		y = 10;
		return y;
	}else if(x == 'K'){
		y = 11;
		return y;
	}else if(x == 'L'){
		y = 12;
		return y;
	}else if(x == 'M'){
		y = 13;
		return y;
	}else if(x == 'N'){
		y = 14;
		return y;
	}else if(x == 'O'){
		y = 15;
		return y;
	}else if(x == 'P'){
		y = 16;
		return y;
	}else if(x == 'Q'){
		y = 17;
		return y;
	}else if(x == 'R'){
		y = 18;
		return y;
	}else if(x == 'S'){
		y = 19;
		return y;
	}else if(x == 'T'){
		y = 20;
		return y;
	}else if(x == 'U'){
		y = 21;
		return y;
	}else if(x == 'V'){
		y = 22;
		return y;
	}else if(x == 'W'){
		y = 23;
		return y;
	}else if(x == 'X'){
		y = 24;
		return y;
	}else if(x == 'Y'){
		y = 25;
		return y;
	}else if(x == 'Z'){
		y = 26;
		return y;
	}else{
		return 0;
	}
}



char Num_Convert(int m){
	int x = m;
	char y = 'A';
	if(x == 1){
		y = 'A';
		return y;
	}else if(x == 2){
		y = 'B';
		return y;
	}else if(x == 3){
		y = 'C';
		return y;
	}else if(x == 4){
		y = 'D';
		return y;
	}else if(x == 5){
		y = 'E';
		return y;
	}else if(x == 6){
		y = 'F';
		return y;
	}else if(x == 7){
		y = 'G';
		return y;
	}else if(x == 8){
		y = 'H';
		return y;
	}else if(x == 9){
		y = 'I';
		return y;
	}else if(x == 10){
		y = 'J';
		return y;
	}else if(x == 11){
		y = 'K';
		return y;
	}else if(x == 12){
		y = 'L';
		return y;
	}else if(x == 13){
		y = 'M';
		return y;
	}else if(x == 14){
		y = 'N';
		return y;
	}else if(x == 15){
		y = 'O';
		return y;
	}else if(x == 16){
		y = 'P';
		return y;
	}else if(x == 17){
		y = 'Q';
		return y;
	}else if(x == 18){
		y = 'R';
		return y;
	}else if(x == 19){
		y = 'S';
		return y;
	}else if(x == 20){
		y = 'T';
		return y;
	}else if(x == 21){
		y = 'U';
		return y;
	}else if(x == 22){
		y = 'V';
		return y;
	}else if(x == 23){
		y = 'W';
		return y;
	}else if(x == 24){
		y = 'X';
		return y;
	}else if(x == 25){
		y = 'Y';
		return y;
	}else if(x == 26){
		y = 'Z';
		return y;
	}else{
		return 'A';
	}
}




int main() {

//Inverse of Transformation

	int Matrix_41[2] = {3,-2};
	int Matrix_42[2] = {1,-1};

	double Inv_Matrix_41[2];
	double Inv_Matrix_42[2];

	int a = Matrix_41[0];
	int b = Matrix_41[1];
	int c = Matrix_42[0];
	int d = Matrix_42[1];

	int tmp;
	tmp = Matrix_41[0];
	Matrix_41[0] = Matrix_42[1];
	Matrix_42[1] = tmp;
	Matrix_41[1] -= 2*Matrix_41[1];
	Matrix_42[0] -= 2*Matrix_42[0];

	int tmp1 = d*a;
	int tmp2 = b*c;
	int disc = tmp1 - tmp2;

	Inv_Matrix_41[0] = (float)Matrix_41[0] / disc;
	Inv_Matrix_41[1] = (float)Matrix_41[1] / disc;
	Inv_Matrix_42[0] = (float)Matrix_42[0] / disc;
	Inv_Matrix_42[1] = (float)Matrix_42[1] / disc;

	printf("Step 1: \n\n");

	printf("[ ");
	printf("%f ",Inv_Matrix_41[0]);
	printf("%f \n",Inv_Matrix_41[1]);
	printf("  %f",Inv_Matrix_42[0]);
	printf("  %f ]\n",Inv_Matrix_42[1]);
	printf("\n");






	//Assign Matrices
	char Matrix_11[2] = {'Z' ,'I'};
	char Matrix_12[2] = {'V' ,'O'};

	char Matrix_21[2] = {'G' ,'Q'};
	char Matrix_22[2] = {'W' ,'S'};

	char Matrix_31[2] = {'C' ,'S'};
	char Matrix_32[2] = {'L' ,'V'};

	int New_Matrix_11[2];
	int New_Matrix_12[2];

	int New_Matrix_21[2];
	int New_Matrix_22[2];

	int New_Matrix_31[2];
	int New_Matrix_32[2];

	printf("Step 2: \n\n");

	printf("[ ");
	printf("%c ",Matrix_11[0]);
	printf("%c \n",Matrix_11[1]);
	printf("  %c",Matrix_12[0]);
	printf("  %c ]\n",Matrix_12[1]);
	printf("\n");

//Assign Encoding Matrix

	int i;
	for(i=0;i<2;i++){
		New_Matrix_11[i] = Alpha_Convert(Matrix_11[i]);
	}

	for(i=0;i<2;i++){
		New_Matrix_12[i] = Alpha_Convert(Matrix_12[i]);
	}

	for(i=0;i<2;i++){
		New_Matrix_21[i] = Alpha_Convert(Matrix_21[i]);
	}

	for(i=0;i<2;i++){
		New_Matrix_22[i] = Alpha_Convert(Matrix_22[i]);
	}

	for(i=0;i<2;i++){
		New_Matrix_31[i] = Alpha_Convert(Matrix_31[i]);
	}

	for(i=0;i<2;i++){
		New_Matrix_32[i] = Alpha_Convert(Matrix_32[i]);
	}

	printf("Step 3: \n\n");

	printf("[ ");
	printf("%d ",New_Matrix_11[0]);
	printf("%d \n",New_Matrix_11[1]);
	printf("  %d",New_Matrix_12[0]);
	printf("  %d ]\n",New_Matrix_12[1]);
	printf("\n");

	printf("[ ");
	printf("%d ",New_Matrix_21[0]);
	printf("%d \n",New_Matrix_21[1]);
	printf("  %d",New_Matrix_22[0]);
	printf("  %d ]\n",New_Matrix_22[1]);
	printf("\n");

	printf("[ ");
	printf("%d ",New_Matrix_31[0]);
	printf("%d \n",New_Matrix_31[1]);
	printf("  %d",New_Matrix_32[0]);
	printf("  %d ]\n",New_Matrix_32[1]);
	printf("\n");

//Multiply Transformation Matrix

	int Mult_Matrix_11[2];
	int Mult_Matrix_12[2];


	tmp1 = (Inv_Matrix_41[0] * New_Matrix_11[0]);
	tmp2 = (Inv_Matrix_41[1] * New_Matrix_12[0]);

	Mult_Matrix_11[0] = tmp1 + tmp2;

	tmp1 = (Inv_Matrix_41[0] * New_Matrix_11[1]);
	tmp2 = (Inv_Matrix_41[1] * New_Matrix_12[1]);

	Mult_Matrix_11[1] = tmp1 + tmp2;



	tmp1 = (Inv_Matrix_42[0] * New_Matrix_11[0]);
	tmp2 = (Inv_Matrix_42[1] * New_Matrix_12[0]);

	Mult_Matrix_12[0] = tmp1 + tmp2;

	tmp1 = (Inv_Matrix_42[0] * New_Matrix_11[1]);
	tmp2 = (Inv_Matrix_42[1] * New_Matrix_12[1]);

	Mult_Matrix_12[1] = tmp1 + tmp2;

	int Mult_Matrix_21[2];
	int Mult_Matrix_22[2];

	tmp1 = (Inv_Matrix_41[0] * New_Matrix_21[0]);
	tmp2 = (Inv_Matrix_41[1] * New_Matrix_22[0]);

	Mult_Matrix_21[0] = tmp1 + tmp2;

	tmp1 = (Inv_Matrix_41[0] * New_Matrix_21[1]);
	tmp2 = (Inv_Matrix_41[1] * New_Matrix_22[1]);

	Mult_Matrix_21[1] = tmp1 + tmp2;



	tmp1 = (Inv_Matrix_42[0] * New_Matrix_21[0]);
	tmp2 = (Inv_Matrix_42[1] * New_Matrix_22[0]);

	Mult_Matrix_22[0] = tmp1 + tmp2;

	tmp1 = (Inv_Matrix_42[0] * New_Matrix_21[1]);
	tmp2 = (Inv_Matrix_42[1] * New_Matrix_22[1]);

	Mult_Matrix_22[1] = tmp1 + tmp2;

	int Mult_Matrix_31[2];
	int Mult_Matrix_32[2];

	tmp1 = (Inv_Matrix_41[0] * New_Matrix_31[0]);
	tmp2 = (Inv_Matrix_41[1] * New_Matrix_32[0]);

	Mult_Matrix_31[0] = tmp1 + tmp2;

	tmp1 = (Inv_Matrix_41[0] * New_Matrix_31[1]);
	tmp2 = (Inv_Matrix_41[1] * New_Matrix_32[1]);

	Mult_Matrix_31[1] = tmp1 + tmp2;



	tmp1 = (Inv_Matrix_42[0] * New_Matrix_31[0]);
	tmp2 = (Inv_Matrix_42[1] * New_Matrix_32[0]);

	Mult_Matrix_32[0] = tmp1 + tmp2;

	tmp1 = (Inv_Matrix_42[0] * New_Matrix_31[1]);
	tmp2 = (Inv_Matrix_42[1] * New_Matrix_32[1]);

	Mult_Matrix_32[1] = tmp1 + tmp2;

	printf("Step 4: \n\n");

	printf("[ ");
	printf("%d ",Mult_Matrix_11[0]);
	printf("%d \n",Mult_Matrix_11[1]);
	printf("  %d",Mult_Matrix_12[0]);
	printf("  %d ]\n",Mult_Matrix_12[1]);
	printf("\n");


	printf("[ ");
	printf("%d ",Mult_Matrix_21[0]);
	printf("%d \n",Mult_Matrix_21[1]);
	printf("  %d",Mult_Matrix_22[0]);
	printf("  %d ]\n",Mult_Matrix_22[1]);
	printf("\n");


	printf("[ ");
	printf("%d ",Mult_Matrix_31[0]);
	printf("%d \n",Mult_Matrix_31[1]);
	printf("  %d",Mult_Matrix_32[0]);
	printf("  %d ]\n",Mult_Matrix_32[1]);
	printf("\n");

//Convert to Useable Number
	for(i=0;i<2;i++){
		while(Mult_Matrix_11[i] < 1 || Mult_Matrix_11[i] > 26){
			if(Mult_Matrix_11[i] <= 0){
				Mult_Matrix_11[i] += 26;
			}else if(Mult_Matrix_11[i] > 26){
				Mult_Matrix_11[i] -= 26;
			}
		}
	}

	for(i=0;i<2;i++){
		while(Mult_Matrix_12[i] < 1 || Mult_Matrix_12[i] > 27){
			if(Mult_Matrix_12[i] <= 0){
				Mult_Matrix_12[i] += 26;
			}else if(Mult_Matrix_12[i] > 26){
				Mult_Matrix_12[i] -= 26;
			}
		}

	}

	for(i=0;i<2;i++){
		while(Mult_Matrix_21[i] < 1 || Mult_Matrix_21[i] > 27){
			if(Mult_Matrix_21[i] <= 0){
				Mult_Matrix_21[i] += 26;
			}else if(Mult_Matrix_21[i] > 26){
				Mult_Matrix_21[i] -= 26;
			}
		}
	}

	for(i=0;i<2;i++){
		while(Mult_Matrix_22[i] < 1 || Mult_Matrix_22[i] > 27){
			if(Mult_Matrix_22[i] <= 0){
				Mult_Matrix_22[i] += 26;
			}else if(Mult_Matrix_22[i] > 26){
				Mult_Matrix_22[i] -= 26;
			}
		}
	}

	for(i=0;i<2;i++){
		while(Mult_Matrix_31[i] < 1 || Mult_Matrix_31[i] > 27){
			if(Mult_Matrix_31[i] <= 0){
				Mult_Matrix_31[i] += 26;
			}else if(Mult_Matrix_31[i] > 26){
				Mult_Matrix_31[i] -= 26;
			}
		}

	}

	for(i=0;i<2;i++){
		while(Mult_Matrix_32[i] < 1 || Mult_Matrix_32[i] > 27){
			if(Mult_Matrix_32[i] <= 0){
				Mult_Matrix_32[i] += 26;
			}else if(Mult_Matrix_32[i] > 26){
				Mult_Matrix_32[i] -= 26;
			}
		}

	}
	printf("Step 5: \n\n");

	printf("[ ");
	printf("%d ",Mult_Matrix_11[0]);
	printf("%d \n",Mult_Matrix_11[1]);
	printf("  %d",Mult_Matrix_12[0]);
	printf("  %d ]\n",Mult_Matrix_12[1]);
	printf("\n");

	printf("[ ");
	printf("%d ",Mult_Matrix_21[0]);
	printf("%d \n",Mult_Matrix_21[1]);
	printf("  %d",Mult_Matrix_22[0]);
	printf("  %d ]\n",Mult_Matrix_22[1]);
	printf("\n");


	printf("[ ");
	printf("%d ",Mult_Matrix_31[0]);
	printf("%d \n",Mult_Matrix_31[1]);
	printf("  %d",Mult_Matrix_32[0]);
	printf("  %d ]\n",Mult_Matrix_32[1]);
	printf("\n");

	for(i=0;i<2;i++){
		Matrix_11[i] = Num_Convert(Mult_Matrix_11[i]);
	}

	for(i=0;i<2;i++){
		Matrix_12[i] = Num_Convert(Mult_Matrix_12[i]);
	}

	for(i=0;i<2;i++){
		Matrix_21[i] = Num_Convert(Mult_Matrix_21[i]);
	}

	for(i=0;i<2;i++){
		Matrix_22[i] = Num_Convert(Mult_Matrix_22[i]);
	}

	for(i=0;i<2;i++){
		Matrix_31[i] = Num_Convert(Mult_Matrix_31[i]);
	}

	for(i=0;i<2;i++){
		Matrix_32[i] = Num_Convert(Mult_Matrix_32[i]);
	}
	printf("Step 6: \n\n");

	printf("[ ");
	printf("%c ",Matrix_11[0]);
	printf("%c \n",Matrix_11[1]);
	printf("  %c",Matrix_12[0]);
	printf(" %c ]\n",Matrix_12[1]);
	printf("\n");


	printf("[ ");
	printf("%c ",Matrix_21[0]);
	printf("%c \n",Matrix_21[1]);
	printf("  %c",Matrix_22[0]);
	printf(" %c ]\n",Matrix_22[1]);
	printf("\n");


	printf("[ ");
	printf("%c ",Matrix_31[0]);
	printf("%c \n",Matrix_31[1]);
	printf("  %c",Matrix_32[0]);
	printf(" %c ]\n",Matrix_32[1]);
	printf("\n");

	printf("Step 7: \n\n");

	printf("%c%c%c%c %c%c %c%c%c%c%c%c\n",Matrix_11[0],Matrix_11[1],Matrix_12[0],Matrix_12[1],Matrix_21[0],Matrix_21[1],Matrix_22[0],Matrix_22[1],Matrix_31[0],Matrix_31[1],Matrix_32[0],Matrix_32[1]);

	return 0;
}
